# Create:Editor Extensions Group Onboarding Issue for {-name-}, starting {-date-}

Welcome {-name-} to Create:Editor Extensions! We're very excited to have you.

This onboarding issue is meant to supplement your general PeopleOps onboarding issue with Editor Extensions specific information and setup.
There is nothing you need to do with this issue until your *second week*, so feel free to ignore it until then.

## Read / bookmark these important links

- [ ] Team handbook: https://about.gitlab.com/handbook/engineering/development/dev/create/editor-extensions
  - [ ] Who we are: https://about.gitlab.com/handbook/engineering/development/dev/create/editor-extensions/#group-overview
  - [ ] How we communicate: https://about.gitlab.com/handbook/engineering/development/dev/create/editor-extensions/#communication
  - [ ] How we work: https://about.gitlab.com/handbook/engineering/development/dev/create/editor-extensions/#group-processes
- [ ] Direction: https://about.gitlab.com/direction/create/editor_extensions/
- [ ] Planning issue: https://gitlab.com/gitlab-org/editor-extensions/meta/-/issues/?label_name%5B%5D=Planning%20Issue
- [ ] Dashboard: https://app.periscopedata.com/app/gitlab/825329/Editor-Extension-Category-MAU-Metrics
- [ ] Team drive: https://drive.google.com/drive/folders/0AGcnhafV_HMgUk9PVA
- [ ] Developer Onboarding: https://handbook.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee-developer-licenses

## Meet the team

- [ ] Setup coffee chats with:
  - [ ] Engineers on the team
    - [ ] _____  
    - [ ] _____  
    - [ ] _____  
    - [ ] _____  
  - [ ] PM: _____
  - [ ] Tech writer: _____

## Calendar

- [ ] Subscribe to the [team calendar](https://calendar.google.com/calendar/u/0/embed?src=c_673d889354d021f7fa9f20a003b5867185a9bf12989b5eaacbc8b537cc9ef27c@group.calendar.google.com)
- [ ] Subscribe to the [Create stage calendar](https://calendar.google.com/calendar/u/0/embed?src=gitlab.com_rtcej6er2a5n9n9rko21coev44@group.calendar.google.com)
- [ ] Link your `Time Off by Deel` to the team calendar
  - In Slack, start a private message to `Time Off by Deel`.
  - In the **Home** tab, select the **Your events** dropdown list, then select **Calendar Sync**.
  - Under **Additional calendars to include**, select **Add calendar**.
  - For **Calendar ID**, paste in the team calendar: `c_673d889354d021f7fa9f20a003b5867185a9bf12989b5eaacbc8b537cc9ef27c@group.calendar.google.com` and select **Submit**.

## Standup
- [ ] Tag your manager below to send an invite to the daily standup managed through Geekbot.
  - [ ] If posting standup at a specific time interests you, feel free to change that setting. 
    - The default time is 10AM in your local time zone.
    - A video on how to do that can be found [here](https://www.loom.com/share/e202c50c078d49859f536f1272f7816a?sid=e9dc5263-ef4a-4c68-9264-65f094dd5191).
  - A few pieces of information on Geekbot:
    - If you want to pause Geekbot for `X` amount of days, simply type `pause for X days` when not responding to a standup.
    - If you want to specify specific OOO time to not get standups, refer to this [documentation](https://geekbot.com/blog/geekbots-out-of-office-feature-a-step-by-step-guide-to-setting-up-automatic-responses/).

## Permissions

### GitLab

- [ ] Manager: Grant `developer` access to https://gitlab.com/gitlab-org/editor-extensions/team
- [ ] Manager: Grant `maintainer` access to the relevant extension/LS project

### Misc

- [ ] Ensure you've requested an EE (Enterprise Edition) license: https://handbook.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee-developer-licenses (this is important for working with GDK)
- [ ] Check with your manager or engineering manager for the group to determine if you will need access to Google Groups or 1Password Vaults. If so, open a [Google Group/1Password access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=slack_googlegroup_1Passwordgroupvault) for the relevant resources:
  - Google Groups: `editorextensions@gitlab.com`, `vscode@gitlab.com`, `jetbrains@gitlab.com`, `visualstudio@gitlab.com`, `neovim@gitlab.com`, `engineering@gitlab.com`
  - 1Password Vaults: `Editor Extensions`, `JetBrains Plugin`, `Visual Studio Extension`, `VS Code`, `Neovim Plugin`
  - Example Access Request: https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/27191

## Extensions Setup and Onboarding

### VS Code

- [ ] Follow the [setup instructions](https://gitlab.com/gitlab-org/gitlab-vscode-extension#setup)

#### VSCode Onboarding Resources

Here are some resources to help you get started with the VSCode extension development:
- [ ] Contributing Page - [link](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/main/CONTRIBUTING.md)
- [ ] (recent) Codebase introduction Video 3/6/2024 - [link](https://www.youtube.com/watch?v=A-53b315gAo)
- [ ] (older) Codebase introduction Video 12/20/2022 - [link](https://youtu.be/ZNm_pplDp3Y)
- [ ] (older) Codebase introduction Video 6/22/2022 - [link](https://www.youtube.com/watch?v=cDTQxQPJzjI)
- [ ] VSCode Extension Maintainership Training Issue - [link](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/1001)

### JetBrains

- [ ] Install JetBrains toolbox - [instructions](https://www.jetbrains.com/toolbox-app/)
- [ ] Install and activate the IDE of your preference - [instructions](https://docs.google.com/document/d/1C0AwsCsZag5CHUfanYNk2uNgOQsWIJWztLOaNFxmv0c/edit)
- [ ] Install the extension - [instructions](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin#setup)

### Neovim

- [ ] Follow the [setup instructions](https://gitlab.com/gitlab-org/editor-extensions/gitlab.vim/#setup)

### Visual Studio (Windows only)

- [ ] Determine if you will (1) actively work on the extension or (2) just test it
- [ ] (1) & (2): Procure a license for Windows
  - Process: https://about.gitlab.com/handbook/finance/procurement/personal-use-software/#how-do-i-submit-a-request-for-new-individual-use-software
  - Example: https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/1460
- [ ] Set up a Windows environment
  - [ ] (1) Get a Windows laptop
    - [ ] Get an exception to use a Windows device + Visual Stuio Professional
      - Example: https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/exceptions/-/issues/51
    - [ ] Order a Windows laptop using your original laptop order form sent by IT
  - [ ] (2) Procure a Parallels license to get a VM
    - Process: https://about.gitlab.com/handbook/finance/procurement/personal-use-software/#how-do-i-submit-a-request-for-new-individual-use-software
    - Example: https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/1459
- [ ] Install Visual Studio
  - [ ] (1) Visual Studio Professional
  - [ ] (2) [Visual Studio Community edition](https://visualstudio.microsoft.com/vs/community/)
- [ ] Install the extension - [instructions](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension#setup)

## Engineering onboarding

- [ ] Watch [GitLab Duo Coffee Chat: An inside look into the GitLab Language Server powering Code Suggestions](https://www.youtube.com/watch?v=VQlWz6GZhrs)
- [ ] Read the AI architecture: https://docs.gitlab.com/ee/development/ai_architecture.html


