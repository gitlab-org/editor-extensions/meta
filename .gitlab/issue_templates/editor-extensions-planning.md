Release planning for the [Editor Extensions Group](https://about.gitlab.com/handbook/product/categories/#editor-extensions-group)

## Overview - [Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/7088820?label_name[]=group%3A%3Aeditor%20extensions)

## Goals

1.

### Checklist

- [ ] Refine the issues already assigned to the current milestone for correct label hygiene
- [ ] Check/update past milestones for wayward issues
- [ ] Add vision items
- [ ] Add security items
- [ ] Add bugs
- [ ] Carefully check ~"planning priority" items for customers are scheduled or well communicated

### Stable counterparts

- [ ] Technical Writing (@aqualls)

([improve this template?](https://gitlab.com/gitlab-org/editor-extensions/meta/-/blob/main/.gitlab/issue_templates/editor-extensions-planning.md))

/label ~"group::editor extensions" ~"devops::create" ~"section::dev" ~"Planning Issue" ~"type::ignore" ~"Category:Editor Extensions"
/assign @dashaadu @francoisrose @aqualls
